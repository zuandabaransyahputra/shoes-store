import React from 'react';
import { NavLink } from 'react-router-dom';

const Navbar = () => {
  return (
    <>
      <div className="p-4 md:flex flex-col gap-10 w-full h-full">
        <div className="flex flex-col text-four">
          <NavLink to="/">Marketplace</NavLink>
          <NavLink to="/explore">Explore Product</NavLink>
          <NavLink to="/info">Shoes Discount</NavLink>
        </div>
        <div className="flex flex-col text-four">
          <h1 className="text-gray-800 mb-2">My Account</h1>
          <NavLink to="/">Your Order</NavLink>
          <NavLink to="/">Notifications</NavLink>
          <NavLink to="/">Favorites Shoes</NavLink>
          <NavLink to="/">Account Settings</NavLink>
        </div>
      </div>
    </>
  );
};

export default Navbar;
