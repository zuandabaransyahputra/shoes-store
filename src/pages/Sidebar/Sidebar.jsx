import React from 'react';
import Navbar from '../../components/Navbar/Navbar';
import Footer from '../Footer/Footer';

const Sidebar = () => {
  return (
    <>
      <div className="flex-[0.15] border-b-2 border-third">
        <div className="h-full w-full flex gap-10 items-center justify-start px-4">
          <h1 className="text-primary font-bold">Logo</h1>
          <div>
            <h1>Shoes Store</h1>
            <p className="text-sm text-third">
              Sell Your Shoe With The Best Way
            </p>
          </div>
        </div>
      </div>
      <div className="flex-[0.7]">
        <Navbar />
      </div>
      <div className="flex-[0.15]">
        <Footer />
      </div>
    </>
  );
};

export default Sidebar;
