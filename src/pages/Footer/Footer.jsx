import React from 'react';
import { AiOutlineBulb } from 'react-icons/ai';

const Footer = () => {
  return (
    <>
      <div className="h-full w-full hidden md:flex flex-col gap-4 items-start justify-center px-4">
        <h1 className="">Light Mode</h1>
        <div>
          <p className="text-sm text-slate-400">2022 Book Store</p>
        </div>
      </div>
      <div className="flex md:hidden items-center justify-center">
        <AiOutlineBulb size={25} className="cursor-pointer" />
      </div>
    </>
  );
};

export default Footer;
