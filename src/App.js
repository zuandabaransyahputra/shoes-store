import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import Sidebar from './pages/Sidebar/Sidebar';
import Home from './pages/Homepage/Home';

function App() {
  return (
    <Router>
      <div className="flex">
        <div className="hidden md:flex-[0.4] lg:flex-[0.25] md:flex flex-col">
          <Sidebar />
        </div>
        <div className="flex-1 md:flex-[0.6] lg:flex-[0.75]">
          <Routes>
            <Route path="/">
              <Route index element={<Home />} />
            </Route>
          </Routes>
        </div>
      </div>
    </Router>
  );
}

export default App;
