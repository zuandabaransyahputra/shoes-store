/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}",],
  theme: {
    extend: {
      colors: {
        'primary': '#646FD4',
        'secondary': '#F6F54D',
        'third': '#DDDDDD',
        'four': '#CCD1E4'
      },
    },
  },
  plugins: [],
}
